import wget
import tarfile
import os

def prepare_efficientdet():
    links = ['https://tfhub.dev/tensorflow/efficientdet/lite0/detection/1?tf-hub-format=compressed',
        'https://tfhub.dev/tensorflow/efficientdet/lite1/detection/1?tf-hub-format=compressed',
        'https://tfhub.dev/tensorflow/efficientdet/lite2/detection/1?tf-hub-format=compressed',
        'https://tfhub.dev/tensorflow/efficientdet/lite3/detection/1?tf-hub-format=compressed',
        'https://tfhub.dev/tensorflow/efficientdet/lite3x/detection/1?tf-hub-format=compressed',
        'https://tfhub.dev/tensorflow/efficientdet/lite4/detection/2?tf-hub-format=compressed']
    if not os.path.exists('efficientdet'):
        os.mkdir('efficientdet')
    os.chdir('efficientdet')

    print('downloading efficientdet models')
    for link in links:
        print(f'\ndownloading {link}')
        wget.download(link)

    print('extracting efficientdet models')
    for root, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            outname = str(filename).split('_detection')[0]
            os.mkdir(outname)
            with tarfile.open(filename) as tarball:
                tarball.extractall(outname)
            os.remove(filename)
    os.chdir('..')

def prepare_ssd_mobilenet_tf1():
    links = ['http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_coco_2018_01_28.tar.gz',
        'http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v3_small_coco_2020_01_14.tar.gz',
        'http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v3_large_coco_2020_01_14.tar.gz']
    if not os.path.exists('ssd_mobilenet_tf1'):
        os.mkdir('ssd_mobilenet_tf1')
    os.chdir('ssd_mobilenet_tf1')

    print('downloading ssd_mobilenet_tf1 models')
    for link in links:
        print(f'\ndownloading {link}')
        wget.download(link)

    print('extracting ssd_mobilenet_tf1 models')
    for root, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            with tarfile.open(filename) as tarball:
                tarball.extractall()
            os.remove(filename)
    # tarfile creates new directories
    for _, dirnames, _ in os.walk('.'):
        for dirname in dirnames:
            outname = str(dirname).split('_coco')[0]
            os.rename(dirname, outname)
    os.chdir('..')

def prepare_ssd_mobilenet_tf2():
    links = ['http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v2_320x320_coco17_tpu-8.tar.gz',
        'http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v1_fpn_640x640_coco17_tpu-8.tar.gz',
        'http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8.tar.gz',
        'http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8.tar.gz']
    if not os.path.exists('ssd_mobilenet_tf2'):
        os.mkdir('ssd_mobilenet_tf2')
    os.chdir('ssd_mobilenet_tf2')

    print('downloading ssd_mobilenet_tf2 models')
    for link in links:
        print(f'\ndownloading {link}')
        wget.download(link)

    print('extracting ssd_mobilenet_tf2 models')
    for root, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            with tarfile.open(filename) as tarball:
                tarball.extractall()
            os.remove(filename)
    # tarfile creates new directories
    for _, dirnames, _ in os.walk('.'):
        for dirname in dirnames:
            outname = str(dirname).split('_coco')[0]
            os.rename(dirname, outname)
    os.chdir('..')

if not os.path.exists('saved_models'):
    os.mkdir('saved_models')
os.chdir('saved_models')

prepare_efficientdet()
prepare_ssd_mobilenet_tf1()
prepare_ssd_mobilenet_tf2()
os.chdir('..')
